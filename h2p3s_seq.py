
from h2p3s import *
from sys import exc_info
import traceback

TimeIt.DEBUG = False

if __name__ == '__main__':
    np.set_printoptions(suppress=True)
    n = 10
    A_SPD = fem.stratified_heat((n,))[0].tocsc()
    A_SPSD = fem.stratified_heat((n+1,), lim=[[1,n]])[0]
    A_SPSD = A_SPSD.tocsc()[np.ix_(range(1,n+1), range(1,n+1))]
    x_true = (np.arange(n, dtype=float)**2).reshape(n,1)
    Schur.defaults["interface"] = [0, 2, 4]
    for A in [A_SPD, A_SPSD]:
        print("\n\n* A:\n{}".format(A.A))
        #print("x_true:\n{}".format(x_true.T))
        A.dot = TimeIt()(A.dot)
        b = A.dot(x_true)
        #print("b:\n{}".format(b.T))
        

        print("\n** Dense\n")
        for solver in (SpFacto(), 
                       SpFacto(symmetry=True),
                       Pinv()):
            try:
                TimeIt.reset()
                solver.setup(A.A)
                x = solver.dot(b)
                print("***{}".format(solver.toStr(level=2)))
                e = x - x_true
                e2 = np.sqrt(e.T.dot(e))
                r = A.dot(e)
                r2 = np.sqrt(r.T.dot(r))
                print("    ||e||2 = {}".format(e2[0,0]))
                print("    ||r||2 = {}".format(r2[0,0]))
                #print(TimeIt())
            except Exception as err:
                print("*** {}".format(solver.toStr(level=2)))
                traceback.print_exc()
                
        print("\n** Sparse\n")
        for solver in (SpFacto(),
                       Pinv(),
                       Pastix(),
                       Pastix(symmetry=True),
                       Mumps(),
                       ConjGrad(),
                       ConjGrad(maxiter=2),
                       ConjGrad(M=Pastix(), maxiter=2),
                       ConjGrad(M=Mumps(), maxiter=2),
                       Schur(local_solver=SpFacto()),
                       Schur(local_solver=Pastix()),
                       Schur(local_solver=Mumps())):
            try:
                TimeIt.reset()
                solver.setup(A)
                x = solver.dot(b)
                print("*** {}".format(solver.toStr(level=2)))
                e = x - x_true
                e2 = np.sqrt(e.T.dot(e))
                r = A.dot(e)
                r2 = np.sqrt(r.T.dot(r))
                print("    ||e||2 = {}".format(e2[0,0]))
                print("    ||r||2 = {}".format(r2[0,0]))
                #print(TimeIt())
            except Exception as err:
                print("*** {}".format(solver.toStr(level=2)))
                traceback.print_exc()
        

print("\n* Ritz values\n")

A = A_SPSD
b = A.dot(x_true)
cg = ConjGrad(A, ritz=True, maxiter=500)
x = cg.dot(b)
l = cg.ritz_values()
print("** CG:\n   {} {}".format(l.min(), l.max()))
l2 = la.eigh(A.A)[0]
print("** la.eigh:\n   {} {}".format(l2.min(), l2.max()))
